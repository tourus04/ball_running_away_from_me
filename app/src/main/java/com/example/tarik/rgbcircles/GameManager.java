package com.example.tarik.rgbcircles;


import android.graphics.Canvas;
import android.graphics.Paint;

public class GameManager {

    private MainCircle mainCircle;
    private CanvasView canvasView;
    private static int mWidth;
    private static int mHeight;

    public GameManager(CanvasView canvasView, int w, int h) {
        this.canvasView = canvasView;
        mWidth = w;
        mHeight = h;
        initMainCircle();
    }

    public static int getWidth() {
        return mWidth;
    }

    public static int getHeight() {
        return mHeight;
    }


    private void initMainCircle() {
        mainCircle = new MainCircle(mWidth / 2, mHeight / 2);
    }

    public void onDraw() {
     canvasView.drawCircle(mainCircle);
    }

    public void onTouchEvent(int x, int y) {
        mainCircle.moveMainCircleWhenTouchAt(x, y);
    }
}

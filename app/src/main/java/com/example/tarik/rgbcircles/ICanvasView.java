package com.example.tarik.rgbcircles;


public interface ICanvasView {
    void drawCircle(MainCircle circle);
}
